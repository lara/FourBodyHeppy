from CMGTools.TTHAnalysis.analyzers.treeProducerSusyCore import *
from CMGTools.TTHAnalysis.analyzers.ntupleTypes import *

susySingleLepton_globalVariables = susyCore_globalVariables + [


            ##-------- custom jets ------------------------------------------
            NTupleVariable("htJet20", lambda ev : ev.htJet20, help="H_{T} computed from leptons and jets (with |eta|<2.4, pt > 20 GeV)"),
	    NTupleVariable("htJet20j", lambda ev : ev.htJet20j, help="H_{T} computed from only jets (with |eta|<2.4, pt > 20 GeV)"),
            NTupleVariable("mhtJet20", lambda ev : ev.mhtJet20, help="H_{T}^{miss} computed from leptons and jets (with |eta|<2.4, pt > 20 GeV)"),
	    NTupleVariable("htJet25", lambda ev : ev.htJet25, help="H_{T} computed from leptons and jets (with |eta|<2.4, pt > 25 GeV)"),
	    NTupleVariable("htJet25j", lambda ev : ev.htJet25j, help="H_{T} computed from only jets (with |eta|<2.4, pt > 25 GeV)"),
            NTupleVariable("mhtJet25", lambda ev : ev.mhtJet25, help="H_{T}^{miss} computed from leptons and jets (with |eta|<2.4, pt > 25 GeV)"),
            NTupleVariable("htJet30j", lambda ev : ev.htJet30j, help="H_{T} computed from only jets (with |eta|<2.4, pt > 30 GeV)"),
            NTupleVariable("htJet30", lambda ev : ev.htJet30, help="H_{T} computed from leptons and jets (with |eta|<2.4, pt > 30 GeV)"),
	    NTupleVariable("mhtJet30", lambda ev : ev.mhtJet30, help="H_{T}^{miss} computed from leptons and jets (with |eta|<2.4, pt > 30 GeV)"),
            NTupleVariable("mhtJet40", lambda ev : ev.mhtJet40, help="H_{T}^{miss} computed from leptons and jets (with |eta|<2.4, pt > 40 GeV)"),
            NTupleVariable("nSoftBJetLoose25",  lambda ev: sum([(sv.mva>0.3 and (sv.jet == None or sv.jet.pt() < 25)) for sv in ev.ivf]) + len(ev.bjetsMedium), int, help="Exclusive sum of jets with pt > 25 passing CSV medium and SV from ivf with loose sv mva"),
            NTupleVariable("nSoftBJetMedium25", lambda ev: sum([(sv.mva>0.7 and (sv.jet == None or sv.jet.pt() < 25)) for sv in ev.ivf]) + len(ev.bjetsMedium), int, help="Exclusive sum of jets with pt > 25 passing CSV medium and SV from ivf with medium sv mva"),
            NTupleVariable("nSoftBJetTight25",  lambda ev: sum([(sv.mva>0.9 and (sv.jet == None or sv.jet.pt() < 25)) for sv in ev.ivf]) + len(ev.bjetsMedium), int, help="Exclusive sum of jets with pt > 25 passing CSV medium and SV from ivf with tight sv mva"),
            ##------------------------------------------------
 	    NTupleVariable("mtw", lambda ev: ev.mtw, help="mt(l,met)"),
	    NTupleVariable("mtw1", lambda ev: ev.mtw1, help="1- 80*80/2*met*pt"),
	    NTupleVariable("mtw2", lambda ev: ev.mtw2, help="cos (phi)"),



]
susySingleLepton_globalObjects = susyCore_globalObjects.copy()
susySingleLepton_globalObjects.update({
            # put more here
})

susySingleLepton_collections = susyCore_collections.copy()
susySingleLepton_collections.update({

            # put more here
            "genParticles"     : NTupleCollection("genPartAll",  genParticleWithMotherId, 200, help="all pruned genparticles"), # need to decide which gen collection ?
            ## ---------------------------------------------
            "selectedLeptons" : NTupleCollection("LepGood", leptonTypeSusy, 8, help="Leptons after the preselection"),
            "otherLeptons"    : NTupleCollection("LepOther", leptonTypeSusy, 8, help="Leptons after the preselection"),
      	    "selectedTaus"    : NTupleCollection("TauGood", tauTypeSusy, 3, help="Taus after the preselection"),
	    #"selectedIsoTrack"    : NTupleCollection("isoTrack", isoTrackType, 50, help="isoTrack, sorted by pt"),

            ## DegStop:
             
            #"packedGenParticles"  : NTupleCollection("genPartPkd",  packedGenParticleWithMotherIndex, 2000, help="all packed genparticles"),
            "Tracks"              : NTupleCollection("Tracks",      trackTypeSusy, 2000, help="all Tracks from PackedPFCandidates (pt>1) , sorted by pt"),
            #"GenTracks"           : NTupleCollection("GenTracks",   genTrackTypeSusy, 2000, mcOnly=True, help="all Tracks from PackedPFCandidates (pt>1) , sorted by pt"),
            ####

            ##------------------------------------------------
            "cleanJetsAll"       : NTupleCollection("Jet",     jetTypeSusy, 30, help="Cental jets after full selection and cleaning, sorted by pt"),
            ##------------------------------------------------ //jet threshold must be changed here
})

